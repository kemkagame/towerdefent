﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader<T> : MonoBehaviour where T : MonoBehaviour
{
    //Referents in <T>
    public static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();

            }
            else if (instance != FindObjectOfType<T>())
            {
                Destroy(FindObjectOfType<T>());
            }
            //The manager must stay on stage
            DontDestroyOnLoad(FindObjectOfType<T>());

            return instance;

        }



    }
}
