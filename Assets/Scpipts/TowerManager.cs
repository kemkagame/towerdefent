﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TowerManager : Loader<TowerManager> {

    //call instanse TowerBtn
    TowerBottom towerBtnPress;

    //Referent SpriteRender
    SpriteRenderer spriterender;

    void Start()
    {
        spriterender = GetComponent<SpriteRenderer>();
    }


    void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {

            //Where do we click
            Vector2 mouePoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            //is it possible to put a tower
            RaycastHit2D hit = Physics2D.Raycast(mouePoint, Vector2.zero);

            if (hit.collider.tag == "TowerSide")
            {
                hit.collider.tag = "TowerSideFull";

                //Call PlaceTower
                PlaceTower(hit);
            }

            
        }
        if (spriterender.enabled)
        {
            //Call method FollMouse
            FollMouse();
        }
    }


    // Method location of the tower
    public void PlaceTower(RaycastHit2D hit)
    {
        if (!EventSystem.current.IsPointerOverGameObject() && towerBtnPress != null)
        {
            //bild new tower
            GameObject newTower = Instantiate(towerBtnPress.TowerObject);
            //location of the new tower
            newTower.transform.position = hit.transform.position;

            DisableDrag();
        }




    }
    public void SelectedTower (TowerBottom towerselected)
    {
        towerBtnPress = towerselected;

        //Call method EnableDrag
        EnableDrag(towerBtnPress.DragSprite);

        Debug.Log("Press..." + towerBtnPress.gameObject);
    }

    public void FollMouse()
    {
        transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        transform.position = new Vector2(transform.position.x, transform.position.y);

    }

    public void EnableDrag(Sprite sprite)
    {
        //Inclusion pictures
        spriterender.enabled = true;
        //Image true
        spriterender.sprite = sprite;
    }
    public void DisableDrag()
    {
        //Inclusion pictures
        spriterender.enabled = false;
        
    }



}
