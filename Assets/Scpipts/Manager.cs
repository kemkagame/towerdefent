﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : Loader<Manager> {


    
    // Element spawn
    public GameObject SpawnPoints;
    // All Enemy
    public GameObject[] enemis;
    // How many opponents should appear max
    public int maxEnemiesOnScreen;
    //How many opponents should apper
    public int totalEnemies;
    //how many enemies spawn
    public int enemiesPerSpawn;

    //List Enemy
    public List<Enemy> EnemyLits = new List<Enemy>();



   

    const float spawDelay = 0.5f;

    
    // Use this for initialization
    void Start ()
    {
        StartCoroutine(Spawn());
	}
	
	

    //Method Spawn Enemy
    IEnumerator Spawn()
    {
        if(enemiesPerSpawn > 0 && EnemyLits.Count < totalEnemies)
        {
            
            //creating opponents
            for ( int i = 0; i < enemiesPerSpawn; i++)
            {
                if(EnemyLits.Count < maxEnemiesOnScreen)
                {
                    //creating new enemy
                    GameObject newEnemy = Instantiate(enemis[1]) as GameObject;
                    
                    //appearances on point
                    newEnemy.transform.position = SpawnPoints.transform.position;
                   

                }
            }
            yield return new WaitForSeconds(spawDelay);
            StartCoroutine(Spawn());
        }

    }

    //Adversary registration method
    public void RegisterMethod(Enemy enemy)
    {
        EnemyLits.Add(enemy);
    }
    
    //unregister the enemy
    public void UnregisterMethod(Enemy enemy)
    {
        EnemyLits.Remove(enemy);
        Destroy(enemy.gameObject);
    }

    public void DestroyEnemy()
    {
        foreach(Enemy enemy in EnemyLits)
        {
            Destroy(enemy.gameObject);

        }
        //creating new opponents
        EnemyLits.Clear();
    }

   
}
