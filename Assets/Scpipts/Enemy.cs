﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    //where is the adversary
    public int target = 0;
    // point exit
    public Transform exit;
    //all touch points of the enemy
    public Transform[] wayPoints;
    //moving enemy
    public float navigation;

    //health
    public int health;

    //Die
    bool isDead = false; 

    public bool IsDead
    {
        get
        {
            return isDead;
        }
    }



    
    //enemy position
    Transform enemy;
    // update moving enemy
    float navigationTime = 0;

    Collider2D enemyCollider;

    Animator anim;


    // Use this for initialization
    void Start ()
    {
        enemy = GetComponent<Transform>();
        enemyCollider = GetComponent<Collider2D>();
        anim = GetComponent<Animator>();
        //creating new opponents
        Manager.Instance.RegisterMethod(this);
		
	}
	    
	// Update is called once per frame
	void Update ()
    {
        if(wayPoints != null && isDead == false)
        {
            //motion extensions the next point
            navigationTime += Time.deltaTime;

            if(navigationTime > navigation)
            {
                if(target < wayPoints.Length)
                {
                    enemy.position = Vector2.MoveTowards(enemy.position, wayPoints[target].position, navigationTime);
                }
                else
                {
                    //go to the entry point
                    enemy.position = Vector2.MoveTowards(enemy.position, exit.position, navigationTime);
                }
                navigationTime = 0;
            }
        }
		
	}

    //Metod OnTrigger
    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag == "MovingPoint")
        {
            // the next point
            target += 1;

        }
        else if (coll.tag == "Finish")
        {
            Manager.Instance.UnregisterMethod(this);
        }
        else if(coll.tag == "ProjectTile")
        {
            ProjectTile newP = coll.gameObject.GetComponent<ProjectTile>();
            EnemyHit(newP.AttacaDamage);
            Destroy(coll.gameObject);
        }
    }

    public void EnemyHit(int hitPoints)
    {
        if (health - hitPoints < 0)
        {
            //hurn
            health -= hitPoints;
            anim.Play("hurt");
        }
        else
        {
            anim.SetTrigger("DidDie");
            //die
            Dead();
            
        }
    }

    public void Dead ()
    {
        isDead = true;
        enemyCollider.enabled = false;
    }
 }
