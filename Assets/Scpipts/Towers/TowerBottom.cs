﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBottom : MonoBehaviour {

    public GameObject towerObject;

    [SerializeField]
    Sprite dragSprite;

    public GameObject TowerObject
    {
        get
        {
            return towerObject;
        }
    }

    public Sprite DragSprite
    {
        get
        {
            return dragSprite;
        }
    }
}
