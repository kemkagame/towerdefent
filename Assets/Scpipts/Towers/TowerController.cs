﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour {

    //delay before firing
    public float timeBetweenAttack;

    // firing range
    public float attackRadius;

    //type of shell
     public ProjectTile projectile;

    Enemy targetEnemy = null;
    // shooting counter
    float attackCounter;

    bool isAttaking = false;


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update()

    {
        attackCounter -= Time.deltaTime;

        if (targetEnemy == null || targetEnemy.IsDead)
        {
            Enemy nearestEnemy = GetNearestEnemy();
            if (nearestEnemy != null && Vector2.Distance(transform.position,
                nearestEnemy.transform.position) <= attackRadius) 
            {
                targetEnemy = nearestEnemy;

            }
        }
        else
        {
            if(attackCounter <=0)
            {
                isAttaking = true;

                attackCounter = timeBetweenAttack;
            }
            else
            {
                isAttaking = false;
            }

            if (Vector2.Distance(transform.localPosition, targetEnemy.transform.localPosition) > attackRadius)
            {
                targetEnemy = null;
            }
        }

    }

    public void FixedUpdate()
    {
        if(isAttaking == true)
        {
            Attack();
        }
    }
    //Method attack tower
    public void Attack()
    {

        isAttaking = false;

        ProjectTile newProjectTile = Instantiate(projectile) as ProjectTile;
        newProjectTile.transform.localPosition = transform.localPosition;

        if (targetEnemy == null)
        {
            Destroy(newProjectTile);
        }
        else
        {
            //move projecttile to enume
            StartCoroutine(Moveprojectile(newProjectTile));
        }
    }

    //Method Move Projectile
    IEnumerator Moveprojectile(ProjectTile projectTile)
    {
        while (GetTargetDitanse(targetEnemy) < 0.20f
            && projectTile != null
            && targetEnemy != null)
        {
            var dir = targetEnemy.transform.localPosition - transform.localPosition;
            var angleDirection = Mathf.Atan2(dir.x, dir.y) * Mathf.Rad2Deg;
            projectile.transform.rotation = Quaternion.AngleAxis(angleDirection, Vector3.forward);
            projectile.transform.localPosition = Vector2.MoveTowards(projectile.transform.localPosition,
                targetEnemy.transform.localPosition, 5f * Time.deltaTime);
            yield return null;
        }

        if (projectile != null || targetEnemy == null)
        {
            Destroy(projectile);

        }


    }

    //
    //state to the goal
    private float GetTargetDitanse (Enemy thisEnemy)
    {
        if(thisEnemy == null)
        {
            thisEnemy = GetNearestEnemy();

            if(thisEnemy == null)
            {
                return 0f;
            }
        }

        return Mathf.Abs(Vector2.Distance(transform.localPosition, thisEnemy.transform.localPosition));
    }
    //what are the opponents in the zone of steam
    private List<Enemy> GetEnemiesInRange()
    {
        List<Enemy> enemeisInRange = new List<Enemy>();

        foreach (Enemy enemy in Manager.Instance.EnemyLits)
        {
            if(Vector2.Distance(transform.localPosition, enemy.transform.localPosition)<= attackRadius)
            {
                enemeisInRange.Add(enemy);
            }
        }
        return enemeisInRange;
    }

   private Enemy GetNearestEnemy()
   {
        Enemy nearestEnemy = null;

        //less than everything
        float smallDistanse = float.PositiveInfinity;

        foreach (Enemy enemy in GetEnemiesInRange())
        {
            if (Vector2.Distance(transform.localPosition, enemy.transform.localPosition) < smallDistanse)
            {
                smallDistanse = Vector2.Distance(transform.localPosition, enemy.transform.localPosition);
                nearestEnemy = enemy;
            }

        }
        return nearestEnemy;


    }
}
